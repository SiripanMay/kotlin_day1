abstract class Animals(var noOfLegs:String,var food:String){
    abstract fun soundAnimal():String
    open fun getSound(): String {
        return "$noOfLegs $food"
    }
}

class Dog(noOfLegs: String,food: String,var name: String):Animals(noOfLegs,food){
    override fun soundAnimal(): String {
        return super.getSound()+" and sound is Box Box"
    }
}

class Lion(noOfLegs: String,food: String):Animals(noOfLegs,food){
    override fun soundAnimal(): String {
        return super.getSound()+" and sound is Meaw Meaw"
    }
}

fun main(args: Array<String>) {
    var Animal1:Dog=Dog("4","Bone","myDog")
    var Animal2:Lion=Lion("4","Meat")
    println(Animal1.soundAnimal())
    println(Animal2.soundAnimal())

}