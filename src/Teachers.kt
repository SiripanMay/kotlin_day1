data class Teacher(var name: String,var courseName:String,var shirtColor:Color)
enum class Color{
    RED,GREEN,BLUE
}

fun main(args: Array<String>) {
    var teacher1:Teacher=Teacher("Siripan","Thai",Color.BLUE)
    var teacher2:Teacher=Teacher("Siripan","Thai",Color.GREEN)
    var teacher3:Teacher=Teacher("Siripan","Math",Color.RED)
    var adHoc = object {
        var x:Int=0
        var y:Int=1
    }
    println(adHoc)
    println(adHoc.x+adHoc.y)

    println(teacher1)
    println(teacher2)
    println(teacher3)
    println(teacher1.equals(teacher3))
    println(teacher1.equals(teacher2))
    println(teacher1.name)
    println(teacher1.component2())
    val (teacherName,teacherCourse)=teacher3
    println("$teacherName teach $teacherCourse")
    var teacher4 = teacher1.copy(courseName = "Math")
    println(teacher4)
}