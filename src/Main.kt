 abstract class Person (var name:String, var surname:String,var gpa:Double){
     constructor(name: String,surname: String): this(name,surname,0.0){

     }
     abstract fun goodBoy():Boolean
     open fun getDetails():String {
         return "$name $surname has score $gpa"
     }
 }

 class Student(name:String,surname: String,gpa: Double,var department:String) : Person(name,surname,gpa){
     override fun goodBoy(): Boolean {
         return gpa>2.0
     }
 }
 


 fun main(args: Array<String>) {
     val no1:Student=Student("Siripan","Saecheah",3.55,"MMIT")
//     val no2:Person=Person("May","Naja")
//     val no3:Person=Person(surname="Saecheah",name="Siripan")
     println(no1.getDetails())
//     println(no2.getDetails())
//     println(no3.getDetails())
 }