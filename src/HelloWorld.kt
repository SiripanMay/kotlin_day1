fun main(args: Array<String>) {
//    helloWorld()
//    helloMore("Prayuth")
//    helloMore("Sunchai")
//    myName("Siripan" ,"Saecheah")
//    myName()
//    myName(name="siripan")
//    myName(surname = "saecheah")
//    gradeReport("Siripan",3.33)
//    gradeReport()
//    gradeReport(name ="May")
//    gradeReport(gpa=3.50)
//    println(isOdd(5))
//    println(isOdd(2))
//    println(getAbbreviation('A'))
//    println(getAbbreviation('B'))
//    println(getAbbreviation('C'))
//    println(getAbbreviation('D'))
//    println(getAbbreviation('F'))
//    println(getGrade(49))
//    println(getGrade(60))
//    println(getGrade(78))
//    println(getGrade(89))
//    println(getGrade(101))
//    println(showGrade(50))
    var arrays = arrayOf(5, 55, 200, 1, 3, 5, 7)
    var max = findMaxValue(arrays)
    println("max value is $max")


}

fun helloWorld(): Unit {
    println("Hello World")
}

fun helloMore(text: String): Unit {
    println("Hello $text")
}

fun myName(name: String = "Your name", surname: String = "Your Surname"): Unit {
    println("$surname $name")
}

fun gradeReport(name: String = "annonymous", gpa: Double = 0.00): Unit = println("masster $name gpa:is $gpa")

fun isOdd(value: Int): String {
    if (value.rem(2) == 0) {
        return "$value is even value"
    } else {
        return "$value is odd value"
    }
}

//fun showGrade(score: Int ): Unit {
//    var grade = getGrade(score)
//    if (grade==null){
//       println("This null")
//    }
//    else{
//        println(getAbbreviation(grade.toCharArray()[0]))
//    }
//
//}

fun showGrade(score: Int): String {
    var grade = getGrade(score)
    if (grade == null) {
        return "This null"
    } else {
        return getAbbreviation(grade.toCharArray()[0])
    }

}

fun getAbbreviation(abbr: Char): String {
    when (abbr) {
        'A' -> return "Abnormal"
        'B' -> return "Bad Boy"
        'F' -> return "Fantastic"
        'C' -> {
            println("not smart")
            return "Cheap"
        }
        else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade: String?
    when (score) {
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 70..80 -> grade = "B"
        in 80..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun findMaxValue(values: Array<Int>): Int {
    var max: Int = 0
    for (i in values.indices) {
        if (max <= values[i]) {
            max = values[i]

        }
    }
    return max
}



